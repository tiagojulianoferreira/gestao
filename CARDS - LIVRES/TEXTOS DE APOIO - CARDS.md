## TEXTOS DE APOIO PARA INFLUENCERS/ATIVISTAS

- Participar das políticas públicas é direito de toda a população brasileira; 
- E agora chegou nossa vez de dizer o que a gente quer para o Brasil nos próximos quatro anos; 
- É a gente dizendo como o governo deve usar o dinheiro do povo;
- A gente precisa ocupar a plataforma Brasil Participativo até 14 de julho e contribuir com o Plano Plurianual que define as políticas do governo;
- As propostas e programas mais votados vão virar ações reais do governo e podem se transformar em lei se aprovadas pelo Congresso Nacional;
- O povo tem voz! E um Brasil com a cara do seu povo só é possível com participação social;
- Acesse: gov.br/brasilparticipativo e participe!

### Hashtags

#BrasilParticipativo #PPAParticipativo #PPA

Obs: Se possível, legendar o vídeo.